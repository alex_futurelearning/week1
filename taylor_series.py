import sys
import math
import time

import numpy as np
import matplotlib.pyplot as plt


def factorial(n):
    """
    Calculate factorial of given n.
    factorial(666) in iterative took 0.085 ms
    :param n:
    :return:
    """

    facto = 1
    for i in range(1, n+1):
        facto *= i
    return facto


def power(x, y):
    """
    calculate optimized power - https://en.wikipedia.org/wiki/Exponentiation_by_squaring
    :param x:
    :param y:
    :return:
    """
    if y == 0:
        return 1
    elif y == 2:
        return x * x
    elif y % 2 == 0:
        return power(power(x, 2), y / 2)
    else:
        return x * power(power(x, 2), (y - 1) / 2)


def sin(x, order=50):
    """
    calculate sin(x) using taylor series of given order.
    https://www.mathsisfun.com/algebra/taylor-series.html
    :param x:
    :param order:
    :return:
    """
    sn = 0

    for i in range(order):
        a = power(-1, i) * power(x, 2 * i + 1)
        b = factorial(2 * i + 1)

        sn += a / b

    return sn


# =================================================== #

def plot_sin(test_values):
    """
    Function compares values of
    :param test_values:
    :return:
    """
    # vals = [0.01 * math.pi, 0.5 * math.pi, 100 * math.pi]

    my_sin = [sin(val) for val in test_values]
    python_sin = [math.sin(val) for val in test_values]

    # print('Given Values: {}'.format(vals))
    # print('Mine: {}'.format(my_sin))
    # print('Python: {}'.format(python_sin))

    plt.plot(test_values, python_sin, marker='+', label='Builtin')
    plt.plot(test_values, my_sin, marker='.', label='Mine')

    plt.legend()
    plt.show()


# =================================================== #


if __name__ == '__main__':
    # arg = int(sys.argv[1])
    #
    # print('!{}={}'.format(arg, factorial(arg)))
    # print('{}**{}={}'.format(3, 3, power(3, 2)))

    values = np.linspace(-10 * math.pi, 10 * math.pi, 10)
    plot_sin(values)
    # time1 = time.time()
    # ret = factorial(666)
    # time2 = time.time()
    # print('{:s} function took {:.3f} ms'.format(factorial.__name__, (time2 - time1) * 1000.0))
